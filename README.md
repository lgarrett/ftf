Copy host_vars/localhost/local_config.yml.template to local_config.yml and
configure your privilege escalation method for the local machine there.

Running `ansible-playbook bootstrap_buster -K` will bootstrap a libvirt/qemu VM that
runs Debian buster for you. It will contain the latest security updates.

Running `ansible-playbook bootstrap_win11.yml -K` will bootstrap a Windows 11
trial VM. Note that the provisioning is very resource hungry, > 50 GB free space
is needed. After the run it will have the latest Windows security update
applied.

Both machines can be interfaced via guest agent.
